package main

import (
	"fmt"
	"github.com/golang/glog"
	"github.com/kr/pty"
	"gitlab.com/liaoyw1/kda/util"
	"io"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
)

type Server struct {
	// docker run args
	image string
	extraArgs string

	// PTYs of debug session
	// key is debug container id
	sessions map[string]*os.File
	sessionMutex sync.Mutex

	httpSvr *http.Server
}


func NewServer(image, extraArgs, serverAddr string) (*Server, error) {
	err := util.CheckDocker()
	if err != nil {
		return nil, err
	}
	return  &Server{
		image: image,
		extraArgs: extraArgs,
		httpSvr: &http.Server{Addr: serverAddr},
		sessions: make(map[string]*os.File),
	}, nil
}

func (s *Server) Start() {
	http.HandleFunc("/debug", s.handleDebug)
	http.HandleFunc("/resize", s.handleResize)
	glog.Info("Start agent server on: " + s.httpSvr.Addr)
	if err := s.httpSvr.ListenAndServe(); err != nil {
		glog.Info("http server error: ", err)
	}
}

func (s *Server) Destroy() {
	 s.sessionMutex.Lock()
	 defer s.sessionMutex.Unlock()

	for containerId := range s.sessions {
		util.RemoveContainer(containerId)
	}
	glog.Info("shutdown http server now.")
	s.httpSvr.Shutdown(nil)
}

func handleHttpErr(w http.ResponseWriter, statusCode int, msg string) {
	w.WriteHeader(http.StatusBadRequest)
	w.Write([]byte(msg))
	glog.Error(msg)
}

func (s *Server) handleDebug(w http.ResponseWriter, r *http.Request) {
	targetId := r.FormValue("targetId")
	debugCommand := r.FormValue("debugCommand")
	glog.Infof("new debug session for %s: %s\n", targetId, debugCommand)
	if targetId == "" {
		handleHttpErr(w, http.StatusBadRequest, "empty container id")
		return
	}

	if _, ok := r.Header["Upgrade"]; !ok {
		handleHttpErr(w, http.StatusBadRequest, "only accept upgrade connection for debug: " + targetId)
		return

	}

	// create container
	output, err := util.RunCmd("docker create -it --net container:%s --pid container:%s %s %s %s",
		targetId, targetId, s.extraArgs, s.image, debugCommand)
	if err != nil {
		handleHttpErr(w, http.StatusInternalServerError, "error creating debug container:\n" + output)
		return
	}
	output = strings.TrimRight(output, "\n")
	outLines := strings.Split(output, "\n")
	containerId :=  outLines[len(outLines) -1]
	glog.Infof("Create container for %s: %s\n", targetId, containerId)

	s.startDebug(w, r, containerId, targetId)
}


func getWinSizeFromRequest(r * http.Request,  containerId string) *pty.Winsize {
	rowsStr := r.FormValue("rows")
	colsStr := r.FormValue("cols")
	if rowsStr != "" && colsStr != "" {
		rows, err1 := strconv.Atoi(rowsStr)
		cols, err2 := strconv.Atoi(colsStr)
		if err1 != nil || err2 != nil {
			glog.Warningf("bad rows(%s, err=%v) cols(%s, err=%v) for debugging %s\n",
				rowsStr, err1, colsStr, err2, containerId)
		} else {
			return &pty.Winsize{
				Rows: uint16(rows),
				Cols: uint16(cols),
			}
		}
	}
	return nil
}


func (s *Server) setSession(containerId string, ptmx *os.File) {
	s.sessionMutex.Lock()
	s.sessions[containerId] = ptmx
	s.sessionMutex.Unlock()
}
func (s *Server) startDebug(w http.ResponseWriter, r *http.Request, containerId, targetContainerId string) {
	defer func() {
		s.sessionMutex.Lock()
		s.sessionMutex.Unlock()
		delete(s.sessions, containerId)
		util.RemoveContainer(containerId)
	}()

	cmd, _ := util.CreateCmd("docker start -ai " + containerId)
	ptmx, err := pty.StartWithSize(cmd, getWinSizeFromRequest(r, containerId))
	if err != nil {
		handleHttpErr(w, http.StatusInternalServerError, fmt.Sprintf("error start debug container for %s: %v", targetContainerId, err))
		return
	}
	s.setSession(containerId, ptmx)

	inStream, outStream, err := util.HijackConnection(w)
	if err != nil {
		handleHttpErr(w, http.StatusInternalServerError, fmt.Sprintf("get underline connection for debug: %s :%v", targetContainerId, err))
		return
	}
	defer util.CloseStreams(inStream, outStream)

	fmt.Fprint(outStream, fmt.Sprintf("HTTP/1.1 101 UPGRADED\r\nContent-Type: application/vnd.kubectl-debug.raw-stream\r\n"+
		"Connection: Upgrade\r\nUpgrade: tcp\r\nDebug-Id:%s\r\n", containerId))
	// copy headers that were removed as part of hijack
	if err := w.Header().WriteSubset(outStream, nil); err != nil {
		handleHttpErr(w, http.StatusInternalServerError, "writer headers")
		return
	}
	fmt.Fprint(outStream, "\r\n")


	// streaming connection
	done := make(chan error, 1)
	cp := func(out io.Writer, in io.Reader) {
		_, err := io.Copy(out, in)
		done <- err
	}
	go cp(ptmx, inStream)
	go cp(outStream, ptmx)
	if err = <-done; err != nil {
		glog.V(4).Info("streaming closed: ", err)
	}
}

func (s *Server) handleResize(w http.ResponseWriter, r *http.Request) {
	containerId := r.FormValue("debugId")
	winSize := getWinSizeFromRequest(r, containerId)
	glog.Infof("request winsize for %s: %#v\n", containerId, winSize)
	if containerId == "" || winSize == nil {
		handleHttpErr(w, http.StatusBadRequest,
			fmt.Sprintf("empty container id(%s) or bad winsize(%v)", containerId, winSize))
		return
	}
	s.sessionMutex.Lock()
	ptmx, ok := s.sessions[containerId]
	s.sessionMutex.Unlock()
	if !ok {
		handleHttpErr(w, http.StatusBadRequest, "debug sesson not exists: " + containerId)
		return
	}

	err := pty.Setsize(ptmx, winSize)
	if err != nil {
		handleHttpErr(w, http.StatusInternalServerError, fmt.Sprintf("change size of %s: %v", containerId, err))
		return
	}
	w.Write([]byte("ok"))
}
