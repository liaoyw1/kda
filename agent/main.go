package main

import (
	"flag"
	"github.com/golang/glog"
	"os"
	"os/signal"
	"syscall"
)

var image = flag.String("image", "alpine", "docker image for the debug container")
var extraArgs = flag.String("extraArgs", "--cpus 1 -m 1g", "extra args for docker cli when running debug container")
var serverAddr = flag.String("serverAddr", ":4242", "listen address for agent server")

func main() {
	initFlags()
	server, err := NewServer(*image, *extraArgs, *serverAddr)
	if err != nil {
		glog.Fatal("Failed to create server: ", err)
	}
	go handleSignal(server)
	server.Start()

}
func handleSignal(server *Server) {
	sigCh := make(chan os.Signal)
	signal.Notify(sigCh, syscall.SIGTERM, syscall.SIGINT)
	glog.Infof("receive signal: %v, stop now.\n", <-sigCh)
	server.Destroy()
}

func initFlags() {
	// for glog
	flag.Set("logtostderr", "true")
	flag.Parse()
}
