# kubectl-debug
## 1. build
```
go build ./agent -o debug-agent
go build ./kubectl-debug
```
## 2. usage
**run the command with -h to view help message**

add -v 4 to show more output
### server side
distribute debug-agent to every k8s node

run with --image xxx --extraArgs xxx (eg. --cpus 1 -m 1g --entrypoint=)
### client side
add kubectl-debug to your $PATH

cp plugin.yaml to ~/.kube/plugins/debug # see [kubectl-plugins](https://v1-9.docs.kubernetes.io/docs/tasks/extend-kubectl/kubectl-plugins/)

## TODO
add systemctl service file for agent

add ut

test more edge case
