package util

import (
	"fmt"
	"github.com/golang/glog"
	"io"
	"net/http"
	"os/exec"
	"strings"
)

// Test if docker cli exists
// and dockerd is running
func CheckDocker() error {
	glog.Info("check docker...")
	output, err := RunCmd("docker info")
	if err != nil {
		glog.Error("failed to check docker:\n" + string(output))
	}
	return err
}

func RemoveContainer(id string) {
	glog.Info("remove container: " + id)
	output, err := RunCmd("docker rm -f " + id)
	if err != nil {
		glog.Warningf("error removing container %s(%v): %s\n", id, err, string(output))
	}
}

func CreateCmd(cmdTemplate string, templateArgs ...interface{}) (*exec.Cmd, string) {
	cmdStr := cmdTemplate
	if len(templateArgs) > 0 {
		cmdStr = fmt.Sprintf(cmdTemplate, templateArgs...)
	}
	glog.V(4).Info("create command: " + cmdStr)
	cmdSegs := strings.Split(cmdStr, " ")
	var trimedSegs []string
	for _, seg := range cmdSegs {
		if seg != "" {
			trimedSegs = append(trimedSegs, seg)
		}
	}
	return exec.Command(trimedSegs[0], trimedSegs[1:]...), cmdStr
}

func RunCmd(cmdTemplate string, templateArgs ...interface{}) (string, error){
	cmd, cmdStr := CreateCmd(cmdTemplate, templateArgs...)
	output, err := cmd.CombinedOutput()
	glog.V(4).Infof("Run [%s], result(err=%v):\n %s", cmdStr, err, string(output))
	return string(output), err
}


func HijackConnection(w http.ResponseWriter) (io.ReadCloser, io.Writer, error) {
	conn, _, err := w.(http.Hijacker).Hijack()
	if err != nil {
		return nil, nil, err
	}
	// Flush the options to make sure the client sets the raw mode
	conn.Write([]byte{})
	return conn, conn, nil
}

func CloseStreams(streams ...interface{}) {
	for _, stream := range streams {
		if tcpc, ok := stream.(interface {
			CloseWrite() error
		}); ok {
			tcpc.CloseWrite()
		} else if closer, ok := stream.(io.Closer); ok {
			closer.Close()
		}
	}
}
