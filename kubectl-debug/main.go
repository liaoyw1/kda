package main

import (
	"flag"
	"fmt"
	"github.com/golang/glog"
	"gitlab.com/liaoyw1/kda/util"
	"golang.org/x/crypto/ssh/terminal"
	"io"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"
)


var namespace = flag.String("n", "", "namespace")
var container = flag.String("c", "", "container")
var agentPort = flag.Int("p", 4242, "agent server port")

// for debug usage
var testIp = flag.String("testIp", "localhost", "test ip")
var testCid = flag.String("testCid", "", "container id for test")

func showUsage() {
	fmt.Println("kubectl plugin debug -n <namespace> <pod> -c <container> <debugCommand>")
}

func main() {
	flag.Set("logtostderr", "true")
	flag.Parse()


	if *testIp != "" && *testCid != "" {
		startDebug(*testIp, *testCid, strings.Join(flag.Args()[0:], " "))
		return
	}

	if len(flag.Args()) == 0 {
		showUsage()
		return
	}
	pod := flag.Args()[0]
	debugCommand := ""

	// handle -c after <pod>
	if len(flag.Args()) > 2 && flag.Args()[1] == "-c" {
			*container = flag.Args()[2]
			debugCommand = strings.Join(flag.Args()[3:], " ")
	} else {
		debugCommand = strings.Join(flag.Args()[1:], " ")
	}

	namespaceOpt := ""
	if *namespace != ""{
		namespaceOpt = "-n " + *namespace
	}
	hostIP, err := util.RunCmd("kubectl %s get pod %s -o go-template='{{.status.hostIP}}'", namespaceOpt, pod)
	if err != nil {
		glog.Fatalf("Can't get host IP: %v`\n" , err)
	}
	hostIP = strings.Trim(hostIP, "'")
	if net.ParseIP(hostIP) == nil {
		glog.Fatal("bad host ip of pod: " + hostIP)
	}

	template := "go-template="
	if *container != "" {
		template += fmt.Sprintf(
			`'{{range .status.containerStatuses }}{{ if eq .name "%s" }}{{ .containerID }}{{ end }}{{ end }}'`, *container)
	} else {
		template += `'{{(index .status.containerStatuses 0).containerID}}'`
	}
	cmd, _ := util.CreateCmd("kubectl %s get pod %s -o ", namespaceOpt, pod)
	cmd.Args = append(cmd.Args, template) // args segment should not container space
	out, err := cmd.CombinedOutput()
	if err != nil {
		glog.Fatalf("Can't get container id(err=%v)\n%s", err, string(out))
	}
	containerId := strings.Trim(string(out), "'")
	if containerId == "" || !strings.HasPrefix(containerId, "docker://") {
		glog.Fatalf("Bad container id(%s)\n", containerId)
	}
	containerId = strings.TrimPrefix(containerId, "docker://")

	startDebug(hostIP, containerId, debugCommand)
}

func check(err error, msg string) {
	if err != nil {
		glog.Fatalf("%s: %v\n", msg, err)
	}
}
func startDebug(hostIP , containerId , command string) {
	glog.Infof("Start debug: hostIP=%s, containerId=%s, command=%s\n", hostIP, containerId, command)
	agentAddr := fmt.Sprintf("%s:%d", hostIP, *agentPort)

	conn, err := net.Dial("tcp", agentAddr)
	check(err, "connecting to:" + agentAddr)

	if tcpConn, ok := conn.(*net.TCPConn); ok {
		tcpConn.SetKeepAlive(true)
		tcpConn.SetKeepAlivePeriod(30 * time.Second)
	}

	clientconn := httputil.NewClientConn(conn, nil)
	defer clientconn.Close()

	resp, err := clientconn.Do(createDebugRequest(containerId, command, agentAddr))
	check(err, "send request to agent: ")

	defer resp.Body.Close()
	if resp.StatusCode != http.StatusSwitchingProtocols {
		glog.Info("response body:")
		io.Copy(os.Stdout, resp.Body)
		glog.Fatalf("expect status code: %d, got %d\n", http.StatusSwitchingProtocols, resp.StatusCode)
	}

	debugId := resp.Header.Get("Debug-Id")
	if debugId == "" {
		glog.Warning("no debug id found, can't change terminal size")
	} else {
		go watchTerminalResize(debugId, agentAddr)
	}

	c, _ := clientconn.Hijack()
	fmt.Println("Connecting...")
	oldState, err := terminal.MakeRaw(int(os.Stdin.Fd()))
	if err != nil {
		panic(err)
	}
	defer func() { _ = terminal.Restore(int(os.Stdin.Fd()), oldState) }() // Best effort.

	go func() {
		io.Copy(c, os.Stdin)
	}()

	io.Copy(os.Stdout, c)



}
func watchTerminalResize(debugId, agentAddr string) {
	sigCh := make(chan os.Signal)
	signal.Notify(sigCh, syscall.SIGWINCH)
	for  range  sigCh {
		glog.V(4).Info("terminal changed")
		RequestTerminalResize(debugId, agentAddr)
	}
}
func RequestTerminalResize(debugId, agentAddr string) {
	req, err := http.NewRequest("POST", fmt.Sprintf("http://%s/resize", agentAddr), nil)
	check(err, fmt.Sprintf("create request to: %s/resize", agentAddr))
	req.Host = agentAddr

	q := req.URL.Query()
	q.Add("debugId", debugId)
	addSizeArgsToReq(q)
	req.URL.RawQuery = q.Encode()
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		glog.Warning("error request to resize terminal: ", err)
		return
	}
	defer resp.Body.Close()
	glog.Infof("request to resize teminal get: ")
	io.Copy(os.Stdout, resp.Body)
}

func createDebugRequest(containerId , command, agentAddr string) *http.Request {
	req, err := http.NewRequest("POST", fmt.Sprintf("http://%s/debug", agentAddr), nil)
	check(err, fmt.Sprintf("create request to: %s/debug", agentAddr))
	req.Host = agentAddr
	req.Header.Set("Connection", "Upgrade")
	req.Header.Set("Upgrade", "tcp")

	q := req.URL.Query()
	q.Add("targetId", containerId)
	q.Add("debugCommand", command)
	addSizeArgsToReq(q)
	req.URL.RawQuery = q.Encode()

	return req
}

func addSizeArgsToReq(q url.Values) {
	cols, rows, err := terminal.GetSize(int(os.Stdin.Fd()))
	if err != nil {
		glog.Warning("Can't get terminal size: " + err.Error())
	} else {
		q.Add("rows", strconv.Itoa(rows))
		q.Add("cols", strconv.Itoa(cols))
	}
}

